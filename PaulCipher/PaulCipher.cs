﻿using System;

namespace Kata
{
    public class PaulCipher
    {
        public static bool IsAlpha(char c) => (64 < c && c < 91) || (96 < c && c < 123);

        public static int ToUpper(char c) => (96 < c) ? c - 32 : c;

        public static char ComputeAlpha(int c)
        {
            if (c < 65)
            {
                c += 26;
            }
            else if (90 < c)
            {
                c -= 26;
            }

            return (char) c;
        }

        public static string Encode(string input)
        {
            string output = "";
            char previousAlpha = ' ';

            if (input == null)
                return "";

            foreach (var c in input)
            {
                if (IsAlpha(c))
                {
                    char uppercaseAlpha = (char) ToUpper(c);

                    if (previousAlpha != ' ')
                    {
                        uppercaseAlpha = ComputeAlpha(uppercaseAlpha + previousAlpha - 64);
                        previousAlpha = (char) ToUpper(c);
                        output += uppercaseAlpha;
                    }
                    else
                    {
                        output += uppercaseAlpha;
                        previousAlpha = uppercaseAlpha;
                    }
                }
                else
                {
                    output += c;
                }
            }

            return output;
        }

        public static string Decode(string input)
        {
            string output = "";
            char previousAlpha = ' ';

            if (input == null)
                return "";

            foreach (var c in input)
            {
                if (IsAlpha(c))
                {
                    char uppercaseAlpha = (char) ToUpper(c);

                    if (previousAlpha != ' ')
                    {
                        previousAlpha = uppercaseAlpha = ComputeAlpha(uppercaseAlpha - (previousAlpha - 64));
                        output += uppercaseAlpha;
                    }
                    else
                    {
                        output += uppercaseAlpha;
                        previousAlpha = uppercaseAlpha;
                    }
                }
                else
                {
                    output += c;
                }
            }

            return output;
        }
    }
}

