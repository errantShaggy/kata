﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ObservedPIN
{
    public class Numpad
    {
        public static Dictionary<char, List<char>> AdjacentKeys = new Dictionary<char, List<char>>
        {
            { '1', new List<char>{ '1', '2', '4'} },
            { '2', new List<char>{ '2', '1', '3', '5'} },
            { '3', new List<char>{ '3', '2', '6'} },
            { '4', new List<char>{ '4', '1', '5', '7'} },
            { '5', new List<char>{ '5', '2', '4', '6', '8'} },
            { '6', new List<char>{ '6', '3', '5', '9'} },
            { '7', new List<char>{ '7', '4', '8'} },
            { '8', new List<char>{ '8', '5', '7', '9'} },
            { '9', new List<char>{ '9', '6', '8'} },
            { '0', new List<char>{ '0', '8'} }
        };
    }
    

    public class Kata
    {
        public List<string> GetPINs(string observed)
        {
            if (String.IsNullOrEmpty(observed))
                return new List<string>();

            GenerateNodes(observed.ToList());

            return Node.AllCombinations;
        }

        public void GenerateNodes(List<char> digits)
        {
            var tree = new Node(Numpad.AdjacentKeys[digits.First()], new List<char>(), "", digits.Skip(1).ToList());
        }
    }

    public class Node
    {
        public static List<string> AllCombinations = new List<string>();
        public string CurrentCombination { get; private set; }
        public List<Node> Childs { get; private set; }
        public Node(List<char> currentPossibleDigits, List<char> previousDigits, string combinationSoFar, List<char> nextDigits)
        {
            CurrentCombination = String.Concat(combinationSoFar, currentPossibleDigits.First());

            foreach(var digit in currentPossibleDigits)
            {
                Childs.Add(new Node(Numpad.AdjacentKeys[digit], ))
            }

            CurrentCombination = String.Concat(previousDigits, currentDigit);
        }
    }
}
