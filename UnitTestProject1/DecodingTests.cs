﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata;

namespace Decoding
{
    [TestClass]
    public class DecodingTests
    {
        [TestMethod]
        public void Test_TBM_VLDLN_mTGLK_TUM_HEHCI_HKAW_LBM_QMAY_CSV()
        {
            Assert.AreEqual("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG", PaulCipher.Decode("TBM VLDLN mTGLK TUM HEHCI HKAW LBM QMAY CSV"));
        }
    }
}
