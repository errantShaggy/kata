using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata;

namespace Encoding
{
    [TestClass]
    public class EncodingTests
    {
        [TestMethod]
        public void He1lo()
        {
            Assert.AreEqual("HM1QA", PaulCipher.Encode("He1lo"));
        }

        [TestMethod]
        public void Test_5ddc4ddf_7a47_44d3_8eda_3171860dc938()
        {
            Assert.AreEqual("5DHG4GHJ-7G47-44E3-8IIE-3171860EG938", PaulCipher.Encode("5ddc4ddf-7a47-44d3-8eda-3171860dc938"));
        }





    }
}
